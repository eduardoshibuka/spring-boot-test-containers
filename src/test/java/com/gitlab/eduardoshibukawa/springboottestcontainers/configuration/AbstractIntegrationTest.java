package com.gitlab.eduardoshibukawa.springboottestcontainers.configuration;

import java.util.Map;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(initializers = AbstractIntegrationTest.Initializer.class)
public class AbstractIntegrationTest {
    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        static AppPostgresqlContainer postgres = AppPostgresqlContainer.getInstance();

        @Override
        public void initialize(ConfigurableApplicationContext applicationContext) {
            configurePostgresProperties(applicationContext);
        }

        private void configurePostgresProperties(ConfigurableApplicationContext applicationContext) {
            Map<String, Object> systemProperties = applicationContext.getEnvironment().getSystemProperties();

            systemProperties.put("spring.datasource.url", postgres.getJdbcUrl());
            systemProperties.put("spring.datasource.username", postgres.getUsername());
            systemProperties.put("spring.datasource.password", postgres.getPassword());
            systemProperties.put("spring.datasource.driver-class-name", postgres.getDriverClassName());

            systemProperties.put("spring.flyway.driver", postgres.getDriverClassName());
            systemProperties.put("spring.flyway.url", postgres.getJdbcUrl());
            systemProperties.put("spring.flyway.schema", postgres.getDatabaseName());
            systemProperties.put("spring.flyway.user", postgres.getUsername());
            systemProperties.put("spring.flyway.password", postgres.getPassword());

        }
    }

}
