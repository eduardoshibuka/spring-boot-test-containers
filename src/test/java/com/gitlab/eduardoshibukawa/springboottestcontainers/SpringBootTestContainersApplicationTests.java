package com.gitlab.eduardoshibukawa.springboottestcontainers;

import com.gitlab.eduardoshibukawa.springboottestcontainers.configuration.AbstractIntegrationTest;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringBootTestContainersApplicationTests extends AbstractIntegrationTest {

	@Test
	void contextLoads() {
	}

}
