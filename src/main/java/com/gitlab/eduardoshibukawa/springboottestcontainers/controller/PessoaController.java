package com.gitlab.eduardoshibukawa.springboottestcontainers.controller;

import java.util.ArrayList;
import java.util.List;

import com.gitlab.eduardoshibukawa.springboottestcontainers.model.Pessoa;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("pessoa")
public class PessoaController {
    
    @GetMapping
    public ResponseEntity<List<Pessoa>> getPesoas() {
        return ResponseEntity.ok().body(new ArrayList());
    }
}
